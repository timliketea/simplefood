'use strict';

const { src, dest, watch, parallel, series } = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const del = require('del');
const svgSprite = require('gulp-svg-sprite');
const sourcemaps = require('gulp-sourcemaps');
const fileInclude = require('gulp-file-include');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

// Paths
const srcPath = './src';
const distPath = './dist';

function browsersyncTask() {
  browserSync.init({
    server: {
      baseDir: srcPath,
    },
    notify: false,
  });
}

function watchingTask() {
  watch([`${srcPath}/scss/**/*.scss`, `${srcPath}/modules/**/*.scss`], styles);
  watch([`${srcPath}/js/**/*.js`]).on('change', browserSync.reload);
  watch([`${srcPath}/**/*.html`, `${srcPath}/modules/**/*.html`]).on(
    'change',
    browserSync.reload
  );
  watch([`${srcPath}/images/icons/*.svg`], svgSpritesTask);
  watch([`${srcPath}/modules/**/*.html`], htmlIncludeTask);
}

function styles() {
  return src(`${srcPath}/scss/style.scss`)
    .pipe(
      plumber({
        errorHandler: notify.onError({
          title: 'Styles',
          sound: false,
          message: '<%= error.message %>',
        }),
      })
    )
    .pipe(sourcemaps.init())
    .pipe(scss({ outputStyle: 'compressed' }))
    .pipe(concat('style.min.css'))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ['last 10 versions'],
        grid: true,
      })
    )
    .pipe(sourcemaps.write())
    .pipe(dest(`${srcPath}/css`))
    .pipe(browserSync.stream());
}

function scripts() {
  return src([`${srcPath}/js/main.js`])
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(dest(`${srcPath}/js`))
    .pipe(browserSync.stream());
}

function images() {
  return src(`${srcPath}/assets/images/**/*.*`)
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
        }),
      ])
    )
    .pipe(dest(`${distPath}/assets/images`));
}

const htmlIncludeTask = () => {
  return src([`${srcPath}/modules/*.html`])
    .pipe(
      fileInclude({
        prefix: '@',
        basepath: '@file',
      })
    )
    .pipe(dest(srcPath))
    .pipe(browserSync.stream());
};

function buildTask() {
  return src(
    [
      `${srcPath}/css/style.min.css`,
      `${srcPath}/assets/fonts/**/*`,
      `${srcPath}/assets/icons/**/*`,
      `${srcPath}/assets/images/**/*`,
      `${srcPath}/js/main.js`,
      `${srcPath}/*.html`,
    ],
    { base: srcPath }
  ).pipe(dest(distPath));
}

function cleanDistTask() {
  return del(distPath);
}

function svgSpritesTask() {
  return src(`${srcPath}/assets/icons/sprites/**/*.svg`)
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: '../sprite.svg',
          },
        },
      })
    )
    .pipe(dest(`${srcPath}/assets/icons`));
}

exports.styles = styles;
exports.scripts = scripts;
exports.browsersync = browsersyncTask;
exports.watching = watchingTask;
exports.images = images;
exports.cleanDist = cleanDistTask;
exports.svgSprites = svgSpritesTask;
exports.htmlInclude = htmlIncludeTask;

exports.build = series(cleanDistTask, images, buildTask);

exports.default = parallel(
  htmlIncludeTask,
  styles,
  browsersyncTask,
  watchingTask
);
